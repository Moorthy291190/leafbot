package com.leafBot.pages;



import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.leafBot.testng.api.base.Annotations;


import cucumber.api.java.en.Given;

public class MyHomePage extends Annotations {
  public MyHomePage()
  {
	PageFactory.initElements(driver, this);
	}
  @FindBy(how=How.XPATH, using="//a[@href='/crmsfa/control/leadsMain']") WebElement eleLeadsTab;
  @Given ("Click LeadTab")
  public MyLeadsPage clickLeadsTab()
	{
		click(eleLeadsTab);
		return new MyLeadsPage();
	
	}
}
