package com.leafBot.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.leafBot.testng.api.base.Annotations;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;

public class CreateLeadPage extends Annotations {

	public CreateLeadPage()
	{
		PageFactory.initElements(driver, this); 
	}
	@CacheLookup
	@FindBy(how=How.XPATH,using ="//a[@href='/crmsfa/control/leadsMain']")
	WebElement eleLeadTab;
	@FindBy(how=How.XPATH,using="(//ul[@class='shortcuts']//a)[2]")
	WebElement eleCreateLeadTab;
	@FindBy(how=How.ID, using="createLeadForm_companyName") 
	WebElement eleCompanyName;
	@FindBy(how=How.ID,using="createLeadForm_firstName")
	WebElement eleFirstName;
	@FindBy(how=How.ID,using="createLeadForm_lastName")
	WebElement eleLastName;
	@FindBy(how=How.NAME,using="submitButton")
		
	WebElement eleCreateLeadButton;
	@Given("Click LeadsTab")
	public CreateLeadPage ClickLeadsTab()
	{
	click(eleLeadTab);
		return this;
	}
	@And("Click CreateLeadTab")
	public CreateLeadPage ClickCreate( )
	{
		click(eleCreateLeadTab);
		return this;
	}
	@And("Type CompanyName")
	public CreateLeadPage typeCompanyName(String cName)
	{
		clearAndType(eleCompanyName,cName);
		return this;
	}
	@And("Type FirstName")
	public CreateLeadPage typeFirstName(String fName)
	{
		clearAndType(eleCompanyName,fName);
		return this;
	}
	@And("Type LastName")
	public CreateLeadPage typeLastName(String lName)
	{
		clearAndType(eleCompanyName,lName);
		return this;
	}
	@When("Click CreateLead Button")
	public ViewLeadPage ClickCreateLead()
	{
		click(eleCreateLeadButton);
		return new ViewLeadPage();
	}
	
}
