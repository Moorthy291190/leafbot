package com.leafBot.testcases;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


import com.leafBot.pages.MyHomePage;
import com.leafBot.testng.api.base.Annotations;

public class TestCreateLead extends Annotations{

	@BeforeClass()
	public void setData()
	{
		testcaseName = "TC002 PositiveCreateLead";
		testcaseDec = "CreateLead into leaftaps";
		author = "Moorthy";
		category = "sanity";
		excelFileName = "TC002s";

	}
	@Test(dataProvider="fetchData")
	public void TestRunCreateLead(String cName,String fName,String lName)
	{
		new MyHomePage()
		.clickLeadsTab()
		.clickCreateLeadTab()
		.typeCompanyName(cName)
		.typeFirstName(fName)
		.typeLastName(lName)
		.ClickCreateLead();
	}
}
